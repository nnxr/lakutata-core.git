import 'reflect-metadata'
import {ThreadClass} from '../../lib/multipleRunner/lib/cores/ThreadClass'

export class Thread extends ThreadClass {

	/**
	 * 线程初始化方法
	 * @returns {Promise<void> | void}
	 * @protected
	 */
	protected initialize(): Promise<void> | void {
		//implement in subclass
	}

	/**
	 * 线程启动方法
	 * @returns {Promise<void>}
	 * @protected
	 */
	protected async setup(): Promise<void> {
		await this.initialize()
	}
}