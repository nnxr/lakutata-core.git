import {BaseObject} from './BaseObject'

export class Plugin extends BaseObject {
	/**
	 * 插件激活时调用函数
	 * @protected
	 */
	protected onActivation() {
		//在子类中使用
	}
}