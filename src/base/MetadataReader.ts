import {MetadataReader as InversifyMetadataReader} from 'inversify'
import {interfaces} from 'inversify/lib/interfaces/interfaces'

export class MetadataReader extends InversifyMetadataReader {
	public getConstructorMetadata(constructorFunc: Function): interfaces.ConstructorMetadata {
		return super.getConstructorMetadata(constructorFunc)
	}

	public getPropertiesMetadata(constructorFunc: Function): interfaces.MetadataMap {
		return super.getPropertiesMetadata(constructorFunc)
	}
}