import {Module} from './Module'
import {Application} from '../../Application'
import {getApp} from '../../ApplicationManagement'

export abstract class Workflow {
	private readonly ___instance: Module

	/**
	 * 获取模块实例
	 * @returns {Module}
	 * @protected
	 */
	protected get module(): Module {
		return this.___instance
	}

	/**
	 * 获取App实例
	 * @returns {Application}
	 * @protected
	 */
	protected get app(): Application {
		return getApp()
	}

	constructor(instance: Module) {
		this.___instance = instance
	}

	/**
	 * 执行工作流
	 * @returns {void | Promise<void>}
	 */
	public abstract run(): void | Promise<void>
}