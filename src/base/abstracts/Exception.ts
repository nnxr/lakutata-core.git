import pupa = require('pupa')
import {format} from 'util'

export abstract class Exception extends Error {
	public abstract district: string
	public abstract errno: number | string
	public readonly appId: string = process.env.appId ? process.env.appId : 'Unknown'
	public readonly appName: string = process.env.appName ? process.env.appName : 'Unknown'
	public readonly errMsg: string
	public readonly err: string

	constructor(errArgs: string[] | string) {
		super()
		const _errArgs: string[] = Array.isArray(errArgs) ? errArgs : [errArgs]
		this.message = (() => {
			let errorMessage: string | null = null
			try {
				const argumentRegExs: RegExp[] = []
				for (const errArg of _errArgs) {
					argumentRegExs.push(new RegExp(errArg))
				}
				for (const template of this.templates()) {
					const str = pupa(template, _errArgs)
					let satisfied: boolean = true
					try {
						argumentRegExs.forEach(argumentRegEx => {
							if (satisfied) {
								if (!argumentRegEx.test(str)) satisfied = false
							}
						})
					} catch (e) {
						satisfied = false
					}
					if (satisfied) {
						errorMessage = str
						break
					}
				}
			} catch (e) {
				errorMessage = null
			}
			if (errorMessage) {
				return errorMessage
			} else {
				return format.apply(format, _errArgs)
			}
		})()
		this.errMsg = this.message
		this.err = this.name
	}

	/**
	 * 获取错误名称
	 * @returns {string}
	 */
	public get name(): string {
		return this.constructor.name
	}

	/**
	 * 设定错误信息模板
	 * @returns {string[]}
	 * @protected
	 */
	protected templates(): string[] {
		return []
	}
}
