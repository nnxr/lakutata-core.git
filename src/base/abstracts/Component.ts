import {BaseObject} from '../BaseObject'

export abstract class Component extends BaseObject {
	protected abstract initialize(): Promise<void> | void
}