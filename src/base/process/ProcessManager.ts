import {Process} from './Process'
import {IProcessConstructor} from '../../interfaces/IProcessConstructor'
import {ProcessException} from '../../exceptions/ProcessException'
import {ProxyClass} from '../../lib/multipleRunner/definitions/interfaces/ProxyClass'
import {Process as processCreate} from '../../lib/multipleRunner/lib/Process'

export class ProcessManager<TProcess extends Process = Process> {
	protected readonly processMap: Map<IProcessConstructor<TProcess>, TProcess & ProxyClass> = new Map()

	/**
	 * 创建进程
	 * @param {IProcessConstructor<TProcess>} processConstructor
	 * @returns {Promise<boolean>}
	 */
	public async createProcess(processConstructor: IProcessConstructor<TProcess>): Promise<boolean> {
		if (this.processMap.has(processConstructor)) return false
		const processProxy = await processCreate(processConstructor)
		this.processMap.set(processConstructor, processProxy)
		return true
	}

	/**
	 * 判断进程是否存在
	 * @param {IProcessConstructor<TProcess>} processConstructor
	 * @returns {boolean}
	 */
	public has(processConstructor: IProcessConstructor<TProcess>): boolean {
		return this.processMap.has(processConstructor)
	}

	/**
	 * 获取进程
	 * @param {IProcessConstructor<TProcess>} processConstructor
	 * @returns {TProcess}
	 */
	public get(processConstructor: IProcessConstructor<TProcess>): TProcess {
		if (this.processMap.has(processConstructor)) {
			return this.processMap.get(processConstructor) as TProcess
		}
		throw new ProcessException(`Process [${processConstructor.name}] is not bound`)
	}

	/**
	 * 销毁进程
	 * @param {IProcessConstructor<TProcess>} processConstructor
	 * @returns {Promise<void>}
	 */
	public async destroy(processConstructor: IProcessConstructor<TProcess>): Promise<void> {
		if (this.processMap.has(processConstructor)) {
			await this.processMap.get(processConstructor)?.terminate()
			this.processMap.delete(processConstructor)
		}
	}
}
