import 'reflect-metadata'
import {ProcessClass} from '../../lib/multipleRunner/lib/cores/ProcessClass'

export class Process extends ProcessClass {
	/**
	 * 进程初始化方法
	 * @returns {Promise<void> | void}
	 * @protected
	 */
	protected initialize(): Promise<void> | void {
		//implement in subclass
	}

	/**
	 * 进程启动方法
	 * @returns {Promise<void>}
	 * @protected
	 */
	protected async setup(): Promise<void> {
		process.title = this.constructor.name
		await this.initialize()
	}
}