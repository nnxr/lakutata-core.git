import {setTimeout} from 'timers'

/**
 * 异步延时函数
 * @param {number} milliseconds
 * @returns {Promise<void>}
 */
export async function delay(milliseconds: number): Promise<void> {
	return new Promise(resolve => {
		setTimeout(() => {
			resolve()
		}, milliseconds)
	})
}