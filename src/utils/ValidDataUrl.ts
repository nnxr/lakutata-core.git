/**
 * ya验证DataUrl
 * @param {string} candidate
 * @returns {boolean}
 */
export function validDataUrl(candidate: string): boolean {
	return /^data:([a-z]+\/[a-z0-9-+.]+(;[a-z0-9-.!#$%*+.{}|~`]+=[a-z0-9-.!#$%*+.{}()|~`]+)*)?(;base64)?,([a-z0-9!$&',()*+;=\-._~:@\/?%\s<>]*?)$/i.test(candidate)
}
