import {Module} from './base/abstracts/Module'
import {IComponentsConfig} from './interfaces/IComponentsConfig'
import {IModulesConfig} from './interfaces/IModulesConfig'
import {TPluginsConfig} from './types/TPluginsConfig'
import {Security} from './plugins/Security'
import {Crypto} from './plugins/Crypto'
import {HttpRequest} from './plugins/HttpRequest'
import {Formatter} from './plugins/Formatter'
import {MessagePack} from './plugins/MessagePack'
import {JSON} from './plugins/JSON'
import {TWorkflowsConfig} from './types/TWorkflowsConfig'
import {Logger} from './plugins/Logger'
import {TProcessesConfig} from './types/TProcessesConfig'
import {TThreadsConfig} from './types/TThreadsConfig'
import momentTimezone from 'moment-timezone'
import {Shell} from './plugins/Shell'

export class Application extends Module {
	/**
	 * 应用程序ID
	 * @type {string}
	 */
	public readonly id: string

	/**
	 * 应用程序名称
	 * @type {string}
	 */
	public readonly name: string

	/**
	 * 组件加载完成回调函数
	 * @param {Application} app
	 * @returns {any}
	 */
	protected readonly onComponentsLoaded: (app) => void | Promise<void>

	/**
	 * 模块加载完成回调函数
	 * @param {Application} app
	 * @returns {any}
	 */
	protected readonly onModulesLoaded: (app) => void | Promise<void>

	/**
	 * 默认插件配置列表
	 * @type {TPluginsConfig}
	 * @protected
	 */
	protected readonly plugins: TPluginsConfig = [
		{
			class: Security,
			saltRound: 8
		},
		{
			class: Crypto,
			bits: 512
		},
		{
			class: HttpRequest
		},
		{
			class: MessagePack
		},
		{
			class: JSON
		},
		{
			class: Shell
		},
		{
			class: Formatter,
			dateFormat: 'YYYY-MM-DD HH:mm:ss'
		},
		{
			class: Logger,
			scope: 'Singleton',
			level: 'debug',
			meta: {
				appId: process.env.appId,
				appName: process.env.appName
			},
			transports: []
		}
	]

	/**
	 * 组件配置列表
	 * @type {IComponentsConfig}
	 * @protected
	 */
	protected readonly components: IComponentsConfig = {}

	/**
	 * 模块配置列表
	 * @type {IModulesConfig}
	 * @protected
	 */
	protected readonly modules: IModulesConfig = {}

	/**
	 * 进程配置列表
	 * @type {TProcessesConfig}
	 * @protected
	 */
	protected readonly processes: TProcessesConfig = []

	/**
	 * 线程配置列表
	 * @type {TThreadsConfig}
	 * @protected
	 */
	protected readonly threads: TThreadsConfig = []

	/**
	 * 工作流配置列表
	 * @type {TWorkflowsConfig}
	 * @protected
	 */
	protected readonly workflows: TWorkflowsConfig = []

	/**
	 * 程序时区
	 * @type {string}
	 * @protected
	 */
	protected tz: string = (() => {
		if (!process.env.TZ) {
			process.env.TZ = momentTimezone.tz.guess()
		}
		return process.env.TZ
	})()

	/**
	 * 初始化
	 * @returns {Promise<void>}
	 * @protected
	 */
	protected async initialize(): Promise<void> {
		this.setProperty('id', process.env.appId)
		process.nextTick(async () => {
			await this.container.initialize()
		})
	}

	/**
	 * 获取程序时区
	 * @returns {string}
	 */
	public getTimezone() {
		return this.tz
	}

	/**
	 * 设置程序时区
	 * @param {string} tz
	 */
	public setTimezone(tz: string) {
		process.env.TZ = tz
		this.tz = tz
	}

	/**
	 * 获取应用程序ID
	 * @returns {string}
	 */
	public getID(): string {
		return process.env.appId as string
	}

	/**
	 * 获取应用程序名称
	 * @returns {string}
	 */
	public getName(): string {
		return process.env.appName as string
	}
}
