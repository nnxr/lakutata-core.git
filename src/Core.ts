require('events').captureRejections = true
import 'reflect-metadata'
import {Container} from './base/Container'
import {Application} from './Application'
import {IApplicationConfig} from './interfaces/IApplicationConfig'
import {FatalException} from './exceptions/FatalException'

/**
 * Decorators
 */
export {Injectable, Inject, Configurable} from './decorators/DependencyInjection'
export {Accept, Return} from './decorators/MethodValidation'

/**
 * Interfaces
 */
export {IApplicationConfig} from './interfaces/IApplicationConfig'
export {IComponentsConfig} from './interfaces/IComponentsConfig'
export {IModulesConfig} from './interfaces/IModulesConfig'
export {IConstructor} from './interfaces/IConstructor'
export {IThreadConstructor} from './interfaces/IThreadConstructor'
export {IProcessConstructor} from './interfaces/IProcessConstructor'

/**
 * Types
 */
export {TPluginsConfig} from './types/TPluginsConfig'
export {TWorkflowsConfig} from './types/TWorkflowsConfig'
export {TProcessesConfig} from './types/TProcessesConfig'
export {TThreadsConfig} from './types/TThreadsConfig'

/**
 * Exceptions
 */
export {FatalException} from './exceptions/FatalException'
export {InvalidMethodAcceptException} from './exceptions/InvalidMethodAcceptException'
export {InvalidMethodReturnException} from './exceptions/InvalidMethodReturnException'
export {ValidationException} from './exceptions/ValidationException'
export {TimeoutException} from './exceptions/TimeoutException'
export {CryptoException} from './exceptions/CryptoException'
export {HttpRequestException} from './exceptions/HttpRequestException'
export {ThreadException} from './exceptions/ThreadException'
export {ProcessException} from './exceptions/ProcessException'
export {ModuleNotFoundException} from './exceptions/ModuleNotFoundException'
export {ComponentNotFoundException} from './exceptions/ComponentNotFoundException'

/**
 * Classes
 */
export {Application} from './Application'
export {BaseObject} from './base/BaseObject'
export {Container} from './base/Container'
export {Component} from './base/abstracts/Component'
export {Module} from './base/abstracts/Module'
export {Plugin} from './base/Plugin'
export {Exception} from './base/abstracts/Exception'
export {Workflow} from './base/abstracts/Workflow'
export {Thread} from './base/thread/Thread'
export {ThreadManager} from './base/thread/ThreadManager'
export {Process} from './base/process/Process'
export {ProcessManager} from './base/process/ProcessManager'

/**
 * Plugins
 */
export {HttpRequest} from './plugins/HttpRequest'
export {Crypto} from './plugins/Crypto'
export {Security} from './plugins/Security'
export {Formatter} from './plugins/Formatter'
export {MessagePack} from './plugins/MessagePack'
export {JSON} from './plugins/JSON'

/**
 * Utilities
 */
export {Validator} from './utils/Validator'
export {delay} from './utils/Delay'
export {timeout} from './utils/Timeout'

/**
 * Application management
 */
export {createApp, getApp} from './ApplicationManagement'
