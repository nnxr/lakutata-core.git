import {Exception} from '../base/abstracts/Exception'

export class ValidationException extends Exception {
	public district: string
	public errno: number | string = 'E_VALIDATION'
}
