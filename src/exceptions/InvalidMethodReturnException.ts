import {Exception} from '../base/abstracts/Exception'

export class InvalidMethodReturnException extends Exception {
	public district: string
	public errno: number | string = 'E_INVALID_METHOD_RETURN'
}
