import {Exception} from '../base/abstracts/Exception'

export class ThreadException extends Exception {
	public district: string
	public errno: number | string = 'E_THREAD'
}
