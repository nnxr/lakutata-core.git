import {Exception} from '../base/abstracts/Exception'

export class ComponentNotFoundException extends Exception {
	public district: string
	public errno: number | string = 'E_COMPONENT_NOT_FOUND'

	protected templates(): string[] {
		return [
			'Component \'{0}\' not found'
		]
	}
}
