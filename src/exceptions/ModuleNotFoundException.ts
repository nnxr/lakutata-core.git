import {Exception} from '../base/abstracts/Exception'

export class ModuleNotFoundException extends Exception {
	public district: string
	public errno: number | string = 'E_MODULE_NOT_FOUND'

	protected templates(): string[] {
		return [
			'Module \'{0}\' not found'
		]
	}
}
