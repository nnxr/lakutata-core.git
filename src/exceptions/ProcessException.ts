import {Exception} from '../base/abstracts/Exception'

export class ProcessException extends Exception {
	public district: string
	public errno: number | string = 'E_PROCESS'
}
