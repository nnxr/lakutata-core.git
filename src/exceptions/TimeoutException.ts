import {Exception} from '../base/abstracts/Exception'

export class TimeoutException extends Exception {
	public district: string
	public errno: number | string = 'E_TIMEOUT'
}
