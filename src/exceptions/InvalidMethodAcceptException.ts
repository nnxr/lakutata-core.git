import {Exception} from '../base/abstracts/Exception'

export class InvalidMethodAcceptException extends Exception {
	public district: string
	public errno: number | string = 'E_INVALID_METHOD_ACCEPT'
}
