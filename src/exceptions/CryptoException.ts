import {Exception} from '../base/abstracts/Exception'

export class CryptoException extends Exception {
	public district: string
	public errno: number | string = 'E_CRYPTO'
}
