import {Exception} from '../base/abstracts/Exception'

export class HttpRequestException extends Exception {
	public district: string
	public errno: number | string = 'E_HTTP_REQUEST'

	protected templates(): string[] {
		return [
			'Http request error occurred: {0}',
			'Http request error occurred while using [{1}]: {0}',
			'Http request error occurred while using [{1}] to [{2}]: {0}',
			'Http request error occurred: {0}'
		]
	}
}
