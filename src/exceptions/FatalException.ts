import {Exception} from '../base/abstracts/Exception'

export class FatalException extends Exception {
	public district: string
	public errno: number | string = 'E_FATAL'
}
