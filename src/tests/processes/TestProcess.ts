import {Process} from '../../base/process/Process'

export class TestProcess extends Process {
	protected initialize(): Promise<void> | void {
		console.log('this is test process')
	}

	public async procTest() {
		console.log('this is procTest', process.title)
	}
}
