import {Thread} from '../../base/thread/Thread'

export class TestThread extends Thread {
	protected initialize(): Promise<void> | void {
		console.log('this is test thread')
	}

	public async test(a, b, c, callback) {
		console.log('this is thread test function')
		callback(Date.now())
		return a + b + c + 'sfsdfasdfasdfasdfasdfasdf'
	}
}