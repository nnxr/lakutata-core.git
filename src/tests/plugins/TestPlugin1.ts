import {Plugin} from '../../base/Plugin'
import {Configurable} from '../../Core'

export class TestPlugin1 extends Plugin {

	@Configurable()
	protected readonly test: string = '99999'

	protected onActivation() {
		console.log('8888888888888888888888888888888888')
	}

	public plugintest() {
		return `this is plugin test ${this.test}`
	}
}

declare module '../../Core' {
	interface Application {
		TestPlugin1: TestPlugin1
	}
}