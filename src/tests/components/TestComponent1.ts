import {Component} from '../../base/abstracts/Component'
import {Configurable} from '../../decorators/DependencyInjection'

export class TestComponent1 extends Component {
	@Configurable()
	public gg: any

	protected initialize(): Promise<void> | void {
		console.log('TestComponent1 init')
	}
}