import {Exception} from '../../Core'

export class TestException extends Exception {
	public district: string
	public errno: number | string = 'E_TEST'
}
