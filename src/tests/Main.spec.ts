import 'reflect-metadata'
import {Test2} from './modules/Test2'
import {Test1} from './modules/Test1'
import {Test3} from './modules/Test3'
import {TestComponent1} from './components/TestComponent1'
import {
	Application,
	BaseObject,
	createApp, Exception,
	FatalException, Formatter,
	getApp,
	IApplicationConfig,
	timeout,
	Validator
} from '../Core'
import {TestPlugin1} from './plugins/TestPlugin1'
import {TimeoutException} from '../exceptions/TimeoutException'
import {Security} from '../plugins/Security'
import {Test4} from './modules/Test4'
import {Logger} from '../plugins/Logger'
import {format, transports} from 'winston'
import {TestThread} from './threads/TestThread'
import {TestProcess} from './processes/TestProcess'
import {TestWorkflow} from './workflows/TestWorkflow'
import {TestException} from './exceptions/TestException'

(async () => {
	try {
		const config: IApplicationConfig = {
			id: 'test.app',
			name: 'test app',
			timezone: 'America/Mazatlan',
			processes: [TestProcess],
			threads: [TestThread],
			plugins: [
				{
					class: TestPlugin1,
					test: '66666'
				},
				{
					class: Formatter,
					dateFormat: 'YYYY-MM-DD HH:mm:ss'
				}
				// {
				// 	class: Logger,
				// 	level: 'debug'
				// }
			],
			components: {
				tc1: {
					class: TestComponent1,
					gg: true
				}
			},
			modules: {
				test1: {
					class: Test1,
					test: '1',
					components: {
						tc1: {class: TestComponent1, gg: 'xxxxxxxoooooooo'}
						// xxx: {class: TestComponent1, gg: 'xxxxxxxoooooooo'}
					},
					modules: {
						test1111: {
							class: Test3,
							test: 666
						}
					},
					workflows: [
						async (instance) => {
							console.log('innnnnnn-TEST1')
						},
						() => {
							console.log('ppppppp-TEST1')
						},
						function OOOO() {
							console.log('ooooo-TEST1')
						},
						() => {
							console.log('9999999999999999-TEST1')
						}
					]
				},
				test2: {
					class: Test2,
					test: '3'
				},
				test3: {
					class: Test1,
					test: '2',
					modules: {
						test1111: {
							class: Test3,
							test: 666
						}
					}
				},
				test33: {
					class: Test3,
					test: '2'
				},
				test4: {class: Test4}
			},
			onModulesLoaded: async (app) => {
				console.log('================================================================onModulesLoaded================================================================')
			},
			onComponentsLoaded: async (app) => {
				console.log('================================================================onComponentsLoaded================================================================')
			},
			workflows: [
				async (instance) => {
					console.log('innnnnnn-APPPPPPPPP')
				},
				() => {
					console.log('ppppppp-APPPPPPPPP')
				},
				function OOOO(ins) {
					console.log('ooooo-APPPPPPPPP111111')
				},
				() => {
					console.log('9999999999999999-APPPPPPPPP')
				},
				async (app) => {
					try {
						console.log(app.Modules.get<Test2>('test2').testFunc())
						console.log(app.TestPlugin1.plugintest())
						console.log('emitting')
						console.log(await app.Security.generatePasswordHash('123456'))
						const key = app.Crypto.generateKeyPair()
						console.log(key)
						console.log(app.Crypto.encrypt({name: 'myq'}, key.public))
						console.log(app.Formatter.asPercent(0.0035))
						console.log(app.Formatter.asDateText())
						const buf = app.MessagePack.encode({
							testMessagePack: true,
							text: 'this is a test',
							err: new Error('this is an error')
						})
						console.log(buf.toString('base64'))
						console.log(app.MessagePack.decode(buf))
						app.Logger.emerg('this is emerg', new Error('test err'))
						app.Logger.alert('this is alert')
						app.Logger.crit('this is crit')
						app.Logger.error('this is error')
						app.Logger.warning('this is warning')
						app.Logger.notice('this is notice')
						app.Logger.info('this is info')
						app.Logger.debug('this is debug')
						// new TestThread()
						const tt = app.Threads.get(TestThread)
						const threadResult = await tt.test('a', 'b', 'c', (timestamp: number) => {
							console.log('fn', timestamp)
						})
						console.log('==========> threadResult:', threadResult)
						await app.Threads.destroy(TestThread)
						const tp = app.Processes.get(TestProcess)
						await tp.procTest()
						await app.Processes.destroy(TestProcess)
						const excep = new TestException('hhhh')
						const ssss = app.JSON.stringify(excep)
						const ssssParsed = app.JSON.parse(ssss)
						console.log(ssssParsed)
						console.log('Is parsed object instance of Exception:', ssssParsed instanceof Exception)
						app.Shell.exec('echo "hello world!!!!!"')
					} catch (e) {
						console.error(e)
					}
				}
			]
		}
		const app = await createApp(config)
	} catch (e) {
		console.log(e)
	}
})()
