import {Module} from '../../base/abstracts/Module'
import {IComponentsConfig} from '../../interfaces/IComponentsConfig'
import {IModulesConfig} from '../../interfaces/IModulesConfig'
import {inject} from 'inversify'
import {TestComponent1} from '../components/TestComponent1'
import {TPluginsConfig} from '../../types/TPluginsConfig'
import {Plugin} from '../../base/Plugin'
import {Accept, Return, Validator} from '../../Core'
import {TWorkflowsConfig} from '../../types/TWorkflowsConfig'
import {Configurable} from '../../decorators/DependencyInjection'
import {TThreadsConfig} from '../../types/TThreadsConfig'
import {TProcessesConfig} from '../../types/TProcessesConfig'

export class Test3 extends Module {
	@Configurable()
	private testVal: number

	@inject('tc1')
	protected readonly bbbb: TestComponent1

	public test3Function() {
		return 'this is test3 function'
	}

	public showTestVal() {
		return this.testVal
	}

	@Accept(Validator.Object({
		test: Validator.Bool.notRequired().default(false),
		bbc:Validator.String.notRequired().default('aaacccc')
	}))
	@Return(Validator.Bool.required())
	public ttttt(inp: { test?: any }): any {
		console.log('ttttt@@@@@Accept:',inp)
		return !!inp.test
	}

	protected initialize() {
		this.testVal = Math.random()
		console.log('test3 init', this.testVal, this.bbbb.gg)
	}

	@Configurable()
	plugins: TPluginsConfig<Plugin>
	@Configurable()
	public readonly components: IComponentsConfig
	@Configurable()
	public readonly modules: IModulesConfig
	@Configurable()
	protected readonly workflows: TWorkflowsConfig
	protected readonly threads: TThreadsConfig = []
	protected readonly processes: TProcessesConfig = []
}
