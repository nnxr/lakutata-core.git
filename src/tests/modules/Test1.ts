import {Module} from '../../base/abstracts/Module'
import {Test3} from './Test3'
import {Configurable, Inject} from '../../decorators/DependencyInjection'
import {TestComponent1} from '../components/TestComponent1'
import {IModulesConfig} from '../../interfaces/IModulesConfig'
import {Component} from '../../base/abstracts/Component'
import {IComponentsConfig} from '../../interfaces/IComponentsConfig'
import {TPluginsConfig} from '../../types/TPluginsConfig'
import {Plugin} from '../../base/Plugin'
import {TWorkflowsConfig} from '../../types/TWorkflowsConfig'
import {Workflow} from '../../base/abstracts/Workflow'
import {TThreadsConfig} from '../../types/TThreadsConfig'
import {TProcessesConfig} from '../../types/TProcessesConfig'


class XX extends Workflow {
	public run(): void | Promise<void> {
		console.log('XXXXXXXXXXXXXXXXXXXX', this.app.id)
	}
}

class XX2 extends XX {
	public run(): void | Promise<void> {
		console.log('XXXXXXXXXXXXXXXXXXXX222222222', this.app.id)
	}
}

export class Test1 extends Module {
	@Configurable()
	plugins: TPluginsConfig<Plugin>
	@Configurable()
	readonly components: IComponentsConfig<Component> = {
		xx: {
			class: TestComponent1,
			gg: 'myqqqqq'
		}
	}
	@Configurable()
	readonly modules: IModulesConfig<Module> = {}

	protected readonly threads: TThreadsConfig = []
	protected readonly processes: TProcessesConfig = []

	@Configurable()
	private readonly test: string

	@Inject('tc1')
	protected readonly testComponent1: TestComponent1

	@Inject('test33')
	protected readonly test3: Test3

	private testVal: number

	public showTestVal() {
		return this.testVal
	}

	protected initialize() {
		this.testVal = Math.random()
		// console.log(this.Modules.get('test3'))
		console.log('test1 init', this.testVal, this.test, this.testComponent1.gg, this.test3.ttttt({test: true}))
		console.log('test1111', this.Modules.get<Test3>('test1111').test3Function())
		const componentId = 'tc1'
		if (this.Components.has(componentId)) {
			console.log('ruioweurioweuroweurioweurw', this.Components.get<TestComponent1>(componentId).gg)
			console.log('ruioweurioweuroweurioweurw1111111111', this.Components.tc1.gg)
		}
	}

	@Configurable()
	protected readonly workflows: TWorkflowsConfig = [
		async (instance) => {
			console.log('innnnnnn', this.id)
		},
		() => {
			console.log('ppppppp', this.id)
		},
		function OOOO() {
			console.log('ooooo')
		},
		XX,
		XX2,
		() => {
			console.log('9999999999999999', this.id)
		}
	]
}