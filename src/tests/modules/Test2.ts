import {Module} from '../../base/abstracts/Module'
import {Test1} from './Test1'
import {Configurable, Inject} from '../../decorators/DependencyInjection'
import {multiInject} from 'inversify'
import {Test3} from './Test3'
import {Application} from '../../Application'
import {Component} from '../../base/abstracts/Component'
import {IComponentsConfig} from '../../interfaces/IComponentsConfig'
import {IModulesConfig} from '../../interfaces/IModulesConfig'
import {Plugin} from '../../base/Plugin'
import {TPluginsConfig} from '../../types/TPluginsConfig'
import {getApp} from '../../Core'
import { TWorkflowsConfig } from '../../types/TWorkflowsConfig'
import {TThreadsConfig} from '../../types/TThreadsConfig'
import {TProcessesConfig} from '../../types/TProcessesConfig'

export class Test2 extends Module {
	@Configurable()
    protected workflows: TWorkflowsConfig<Module>
	@Configurable()
	plugins: TPluginsConfig<Plugin>
	@Configurable()
	components: IComponentsConfig<Component>
	@Configurable()
	modules: IModulesConfig<Module>
	protected readonly threads: TThreadsConfig = []
	protected readonly processes: TProcessesConfig = []
	@Inject(Application)
	protected readonly app: Application

	@Inject('test1')
	protected readonly test1: Test1

	@Inject('test1')
	protected readonly test11: Test1

	@multiInject('test3')
	protected readonly testArr: Test1[]

	@Inject('test3')
	protected readonly test13: Test1

	@Inject('test33')
	protected readonly test33: Test3

	public isInit: boolean = false

	public testFunc() {
		console.log(this.classPath, this.className)
		return this.test1.showTestVal()
	}

	protected initialize(): Promise<void> | void {
		console.log('getApp', getApp().id)
		this.isInit = true
		console.log('test2 testVal:', this.test1.showTestVal(), this.test13.showTestVal())
		// return new Promise(resolve => {
		// 	setTimeout(()=>{
		// 		this.isInit = true
		// 		console.log('test2 testVal:', this.test1.showTestVal(), this.test13.showTestVal())
		// 		resolve()
		// 	},10000)
		// })
	}
}