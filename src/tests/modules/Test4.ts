import {IComponentsConfig, IModulesConfig, Module, TPluginsConfig} from '../../Core'
import {TWorkflowsConfig} from '../../types/TWorkflowsConfig'
import {Configurable} from '../../decorators/DependencyInjection'
import {TThreadsConfig} from '../../types/TThreadsConfig'
import {TProcessesConfig} from '../../types/TProcessesConfig'
import {TestWorkflow} from '../workflows/TestWorkflow'

export class Test4 extends Module {
	@Configurable()
	protected readonly components: IComponentsConfig
	@Configurable()
	protected readonly modules: IModulesConfig
	@Configurable()
	protected readonly plugins: TPluginsConfig
	@Configurable()
	protected readonly workflows: TWorkflowsConfig = [TestWorkflow]
	protected readonly threads: TThreadsConfig = []
	protected readonly processes: TProcessesConfig = []
	@Configurable()
	protected readonly config: any

	@Configurable()
	protected readonly config2: any

	protected initialize(): Promise<void> | void {
		console.log('this is TEST4!!!!!!!')
	}
}
