import {ThreadClass} from '../../lib/cores/ThreadClass'
import {ClassConstructor} from './ClassConstructor'

export interface ThreadClassConstructor<T extends ThreadClass> extends ClassConstructor<T> {
    new(...args: any[]): T

    CLASS_PATH: string

    CLASS_NAME: string
}
