import {ProcessClass} from '../../lib/cores/ProcessClass'
import {ClassConstructor} from './ClassConstructor'

export interface ProcessClassConstructor<T extends ProcessClass> extends ClassConstructor<T> {
    new(...args: any[]): T

    CLASS_PATH: string

    CLASS_NAME: string
}
