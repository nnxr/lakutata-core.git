import {MultipleRunnerBaseClass} from '../../lib/prototypes/MultipleRunnerBaseClass'

export interface MultipleRunnerBaseClassConstructor {
    new(...args: any[]): MultipleRunnerBaseClass
}
