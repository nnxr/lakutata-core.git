import {Volume} from 'memfs'

export const BASE_RUNTIME_CONTAINER_FILE: string = '/multipleRunner/container/base/BaseRuntimeContainer.js'
export const PROCESS_RUNTIME_CONTAINER_FILE: string = '/multipleRunner/container/ProcessRuntimeContainer.js'
export const THREAD_RUNTIME_CONTAINER_FILE: string = '/multipleRunner/container/ThreadRuntimeContainer.js'

export const ContainerVolume = Volume.fromJSON({
	[BASE_RUNTIME_CONTAINER_FILE]: `
    module.exports.BaseRuntimeContainer = function (bridge) {
    bridge.on('create', (name, path) => {
        const module = require(path)[name]
        const moduleInstance = new module(bridge)
        moduleInstance.setup().then(() => {
            const properties = {}
            const moduleInstancePropertiesObject = {}
            for (const propertyName of Object.keys(moduleInstance)) {
                properties[propertyName] = typeof moduleInstance[propertyName]
                if (propertyName !== '__bridge__')
                    moduleInstancePropertiesObject[propertyName] = moduleInstance[propertyName]
            }
            const moduleInstanceProxy = new Proxy(moduleInstance, {
                set(target, p, value) {
                    moduleInstance[p] = value
                    bridge.propertyChange(p.toString(), value)
                    return true
                }
            })
            bridge.onSet((property, value) => {
                moduleInstanceProxy[property] = value
            })
            bridge.ready(properties, JSON.parse(JSON.stringify(moduleInstancePropertiesObject)))
            bridge.onInvoke(async (method, args) => {
                const _args = []
                const callbackSignature = /^__________%\\$CALLBACK\\[\\d+]\\$%__________$/
                for (const arg of args) {
                    if (typeof arg === 'string' && callbackSignature.test(arg)) {
                        _args.push((...args) => {
                            bridge.onCallback(arg, args)
                        })
                    } else {
                        _args.push(arg)
                    }
                }
                return await moduleInstanceProxy[method].apply(moduleInstanceProxy, _args)
            })
        }).catch(setupError => {
            bridge.setupError(setupError)
            process.exit(1)
        })
    })
}
    `,
	[PROCESS_RUNTIME_CONTAINER_FILE]: `
    const reversedArgvs = process.argv.reverse()
    require(reversedArgvs[0])
    const {ProcessBridge} = require(reversedArgvs[0])
    const {BaseRuntimeContainer} = require(reversedArgvs[1])
    BaseRuntimeContainer(new ProcessBridge(process))
    `,
	[THREAD_RUNTIME_CONTAINER_FILE]: `
    const reversedArgvs = process.argv.reverse()
    if(reversedArgvs[3]!=='undefined'){
        try{require(reversedArgvs[3])}catch(e){}
    }
    const {parentPort} = require('worker_threads')
    let _ThreadBridge
    try{
        const {ThreadBridge} = require(reversedArgvs[0])
        _ThreadBridge = ThreadBridge
    }catch(e){
        const tsNodeRegExp=new RegExp("ts-node")
        for(const reversedArgv of reversedArgvs){
            if(tsNodeRegExp.test(reversedArgv)){
                require(reversedArgv)
            }
        }
         const {ThreadBridge} = require(reversedArgvs[0])
        _ThreadBridge = ThreadBridge
    }
    const {BaseRuntimeContainer} = require(reversedArgvs[1])
    BaseRuntimeContainer(new _ThreadBridge(parentPort))
    `
})
