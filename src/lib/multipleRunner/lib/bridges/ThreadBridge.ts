import {BaseBridge} from './BaseBridge'
import {Worker} from 'worker_threads'

export class ThreadBridge extends BaseBridge {

	public send(channel: string, ...args: any[]): void {
		try {
			const workerThreadBridge: Worker = this.bridge
			workerThreadBridge.postMessage({
				channel: channel,
				args: args
			})
		} catch (e) {
		}
	}

	public async terminate(): Promise<void> {
		await this.bridge.terminate()
	}
}
