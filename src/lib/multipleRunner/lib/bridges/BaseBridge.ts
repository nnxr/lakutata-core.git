import {EventEmitter} from 'events'
import random from 'random'
import {INVOKE, PROPERTY_CHANGE, READY, SET_PROPERTY, SETUP_ERROR} from '../../constants/Channel'

export class BaseBridge {
	protected readonly eventEmitter: EventEmitter = new EventEmitter()
	protected readonly bridge: any

	constructor(bridgeObject: any) {
		this.bridge = bridgeObject
		this.bridge
			.on('message', data => {
				this.eventEmitter.emit.apply(this.eventEmitter, [data.channel].concat(data.args) as any)
			})
			.on('error', bridgeError => {
			})
	}

	public off(channel: string) {
		this.eventEmitter.removeAllListeners(channel)
		return this
	}

	public on(channel: string, listener: (...args: any[]) => void) {
		this.eventEmitter.on.apply(this.eventEmitter, [channel, listener])
		return this
	}

	public once(channel: string, listener: (...args: any[]) => void) {
		this.eventEmitter.once.apply(this.eventEmitter, [channel, listener])
		return this
	}

	public send(channel: string, ...args: any[]) {
		throw new Error('Method did not implemented')
	}

	public ready(properties: { [key: string]: any }, propertyValues: { [key: string]: any }) {
		this.send(READY, properties, propertyValues)
		return this
	}

	public onReady(callback: (properties: { [key: string]: any }, propertyValues: { [key: string]: any }) => void) {
		this.once(READY, callback)
		return this
	}

	public error(error: Error) {
		this.send('error', error.message)
	}

	public setupError(error: Error) {
		this.send(SETUP_ERROR, error.message)
	}

	public async invoke(method: string, args: Array<any>) {
		return new Promise((resolve, reject) => {
			const invokeId: string = `ts_${Date.now()}_rnd_${random.int(100, 999999)}`
			const responseChannelName: string = `__%$invoke_res_${invokeId}$%__`
			this.once(responseChannelName, (invokeError: Error, result: any) => {
				if (invokeError) {
					reject(invokeError)
				} else {
					resolve(result)
				}
			})
			this.send(INVOKE, method, args, responseChannelName)
		})
	}

	public onInvoke(handler: (method: string, args: Array<any>) => Promise<any>): void {
		this.on(INVOKE, (method: string, args: Array<any>, responseChannelName: string) => {
			handler(method, args).then(result => {
				this.send(responseChannelName, null, result)
			}).catch(invokeError => {
				this.send(responseChannelName, invokeError, undefined)
			})
		})
	}

	public set(property: string, value: any) {
		this.send(SET_PROPERTY, property, value)
		return this
	}

	public onSet(handler: (property: string, value: any) => void) {
		this.on(SET_PROPERTY, handler)
		return this
	}

	public propertyChange(property: string, value: any) {
		this.send(PROPERTY_CHANGE, property, value)
		this.send('property-changed', property, value)
		return this
	}

	public onPropertyChanged(handler: (property: string, value: any) => void) {
		this.on(PROPERTY_CHANGE, handler)
		return this
	}

	public callbackHandler(thisArg: any, handler: (...args: any[]) => any): string {
		const callbackEvtName: string = `__________%$CALLBACK[${Date.now()}]$%__________`
		this.once(callbackEvtName, (...args: any[]) => {
			handler.apply(thisArg, args)
		})
		return callbackEvtName
	}

	public onCallback(callbackEvtName: string, args: any[]) {
		this.send.apply(this, [callbackEvtName].concat(args) as any)
		return this
	}

	public async terminate(): Promise<void> {
		throw new Error('Method did not implemented')
	}
}
