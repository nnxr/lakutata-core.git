import {BaseBridge} from './BaseBridge'
import {ChildProcess} from 'child_process'

export class ProcessBridge extends BaseBridge {

	public send(channel: string, ...args: any[]) {
		const childProcessBridge: ChildProcess = this.bridge
		if (childProcessBridge.connected) {
			childProcessBridge.send({
				channel: channel,
				args: args
			})
		}
	}

	public async terminate(): Promise<void> {
		return new Promise(resolve => {
			this.bridge.once('close', resolve)
			this.bridge.kill()
		})
	}
}
