import 'reflect-metadata'
import {ThreadClassConstructor} from '../definitions/interfaces/ThreadClassConstructor'
import {ThreadClass} from './cores/ThreadClass'
import {Worker} from 'worker_threads'
import path from 'path'
import {ThreadBridge} from './bridges/ThreadBridge'
import {GenerateExecArgv} from './cores/GenerateExecArgv'
import {UniParentBridgeHandler} from './cores/UniParentBridgeHandler'
import {ProxyClass} from '../definitions/interfaces/ProxyClass'
import os from 'os'
import fs from 'fs'
import {
	BASE_RUNTIME_CONTAINER_FILE,
	ContainerVolume,
	THREAD_RUNTIME_CONTAINER_FILE
} from '../containers/ContainerVolume'
import {Process} from './Process'
import {isElectron} from './cores/IsElectron'

export async function Thread<T extends ThreadClass>(threadClassConstructor: ThreadClassConstructor<T>): Promise<T & ProxyClass> {
	if (isElectron()) {
		return await Process(threadClassConstructor as any) as any
	} else {
		return new Promise((resolve, reject) => {
			ThreadProxy(threadClassConstructor, (setupError, instance) => {
				if (setupError) {
					reject(setupError)
				} else {
					resolve(<T & ProxyClass>instance)
				}
			})
		})
	}
}

function ThreadProxy<T extends ThreadClass>(threadClassConstructor: ThreadClassConstructor<T>, callback: (setupError: Error, instance: T) => void) {
	const className: string = threadClassConstructor.CLASS_NAME
	const classPath: string = threadClassConstructor.CLASS_PATH
	const tmpBaseRuntimeContainerFilename: string = path.resolve(os.tmpdir(), `./TBRC_TMP_${Date.now()}.tmp`)
	fs.writeFileSync(tmpBaseRuntimeContainerFilename, ContainerVolume.readFileSync(BASE_RUNTIME_CONTAINER_FILE).toString())
	const exeArgv = GenerateExecArgv(classPath).reverse()
	const worker: Worker = new Worker(ContainerVolume.readFileSync(THREAD_RUNTIME_CONTAINER_FILE).toString(), {
		eval: true,
		argv: [
			exeArgv.length > 0 ? exeArgv[0] : 'undefined',
			tmpBaseRuntimeContainerFilename,
			require.resolve(path.resolve(__dirname, './bridges/ThreadBridge'))
		]
	})
	// @ts-ignore
	UniParentBridgeHandler(new ThreadBridge(worker), className, classPath, threadClassConstructor, callback)
}
