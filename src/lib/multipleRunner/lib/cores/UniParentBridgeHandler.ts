import {BaseBridge} from '../bridges/BaseBridge'
import {SETUP_ERROR} from '../../constants/Channel'

export function UniParentBridgeHandler<T>(bridge: BaseBridge, className: string, classPath: string, classConstructor: any, callback: (setupError: Error, proxyInstance: T) => void): void {
    const reservedMethodNames: Array<string> = ['off', 'on', 'once', 'emit', 'terminate']
    bridge.on(SETUP_ERROR, (setupErrorMsg => {
        callback(new Error(setupErrorMsg), null as any)
    })).onReady((properties: { [key: string]: any }, propertyValues: { [key: string]: any }) => {
        bridge.onPropertyChanged((property, value) => {
            propertyValues[property] = value
        })
        const proxyHandler: any = new Proxy({}, {
            get(target: T, p: PropertyKey): any {
                if (reservedMethodNames.indexOf(p.toString()) > -1) {
                    switch (p.toString()) {
                        case 'off': {
                            return new Proxy((...args: any[]) => {
                            }, {
                                apply(applyTarget: (...args: any[]) => void, thisArg: any, argArray?: any): any {
                                    bridge.off.apply(bridge, argArray)
                                }
                            })
                        }
                        case 'on': {
                            return new Proxy((...args: any[]) => {
                            }, {
                                apply(applyTarget: (...args: any[]) => void, thisArg: any, argArray?: any): any {
                                    bridge.on.apply(bridge, argArray)
                                }
                            })
                        }
                        case 'once': {
                            return new Proxy((...args: any[]) => {
                            }, {
                                apply(applyTarget: (...args: any[]) => void, thisArg: any, argArray?: any): any {
                                    bridge.once.apply(bridge, argArray)
                                }
                            })
                        }
                        case 'emit': {
                            return new Proxy((...args: any[]) => {
                            }, {
                                apply(applyTarget: (...args: any[]) => void, thisArg: any, argArray?: any): any {
                                    bridge.send.apply(bridge, argArray)
                                }
                            })
                        }
                        case 'terminate': {
                            return new Proxy((...args: any[]) => {
                            }, {
                                apply(applyTarget: (...args: any[]) => void, thisArg: any, argArray?: any): any {
                                    return new Promise((resolve, reject) => {
                                        bridge.terminate().then(resolve).catch(reject)
                                    })
                                }
                            })
                        }
                        default:
                            return undefined
                    }
                } else {
                    const method = classConstructor.prototype[p]
                    const isPropertyMethod: boolean = properties[p.toString()] === 'function'
                    if (method || isPropertyMethod) {
                        return new Proxy(isPropertyMethod ? (...args: any[]) => {
                        } : method, {
                            apply(target, thisArg: any, argArray?: any): any {
                                const _argArray: any[] = []
                                for (const _arg of argArray) {
                                    if (typeof _arg !== 'function') {
                                        _argArray.push(_arg)
                                    } else {
                                        _argArray.push(bridge.callbackHandler(thisArg, _arg))
                                    }
                                }
                                return new Promise((resolve, reject) => {
                                    bridge.invoke(p.toString(), _argArray).then(resolve).catch(reject)
                                })
                            }
                        })
                    } else if (properties[p.toString()]) {
                        return propertyValues[p.toString()]
                    } else {
                        return undefined
                    }
                }
            },
            set(target: {}, p: PropertyKey, value: any, receiver: any): boolean {
                bridge.set(p.toString(), value)
                return true
            }
        })
        bridge.off(SETUP_ERROR)
        callback(null as any, proxyHandler)
    })
    bridge.send('create', className, classPath)
}
