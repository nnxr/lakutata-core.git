import 'reflect-metadata'
import {ProcessClassConstructor} from '../definitions/interfaces/ProcessClassConstructor'
import {ChildProcess, fork} from 'child_process'
import path from 'path'
import {GenerateExecArgv} from './cores/GenerateExecArgv'
import {ProcessBridge} from './bridges/ProcessBridge'
import {ProcessClass} from './cores/ProcessClass'
import {UniParentBridgeHandler} from './cores/UniParentBridgeHandler'
import {ProxyClass} from '../definitions/interfaces/ProxyClass'
import {
	BASE_RUNTIME_CONTAINER_FILE,
	ContainerVolume,
	PROCESS_RUNTIME_CONTAINER_FILE
} from '../containers/ContainerVolume'
import fs from 'fs'
import os from 'os'

export async function Process<T extends ProcessClass>(processClassConstructor: ProcessClassConstructor<T>): Promise<T & ProxyClass> {
	return new Promise((resolve, reject) => {
		ProcessProxy(processClassConstructor, (setupError, instance) => {
			if (setupError) {
				reject(setupError)
			} else {
				resolve(<T & ProxyClass>instance)
			}
		})
	})
}

function ProcessProxy<T extends ProcessClass>(processClassConstructor: ProcessClassConstructor<T>, callback: (setupError: Error, instance: T) => void) {
	const className: string = processClassConstructor.CLASS_NAME
	const classPath: string = processClassConstructor.CLASS_PATH
	const tmpProcessRuntimeContainerFilename: string = path.resolve(os.tmpdir(), `./PRC_TMP_${Date.now()}.tmp`)
	const tmpBaseRuntimeContainerFilename: string = path.resolve(os.tmpdir(), `./PBRC_TMP_${Date.now()}.tmp`)
	fs.writeFileSync(tmpBaseRuntimeContainerFilename, ContainerVolume.readFileSync(BASE_RUNTIME_CONTAINER_FILE).toString())
	fs.writeFileSync(tmpProcessRuntimeContainerFilename, ContainerVolume.readFileSync(PROCESS_RUNTIME_CONTAINER_FILE).toString())
	const childProcess: ChildProcess = fork(tmpProcessRuntimeContainerFilename, [
		tmpBaseRuntimeContainerFilename,
		require.resolve(path.resolve(__dirname, './bridges/ProcessBridge'))
	], {
		execArgv: GenerateExecArgv(classPath),
		serialization: 'advanced'
	})
	UniParentBridgeHandler(new ProcessBridge(childProcess), className, classPath, processClassConstructor, callback)
}
