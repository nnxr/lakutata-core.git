import {Module} from '../base/abstracts/Module'
import {IConstructor} from './IConstructor'
import {IComponentsConfig} from './IComponentsConfig'
import {TWorkflowsConfig} from '../types/TWorkflowsConfig'
import {TThreadsConfig} from '../types/TThreadsConfig'
import {TProcessesConfig} from '../types/TProcessesConfig'

export interface IModulesConfig<TModule extends Module = Module> {
	[key: string]: {
		class: IConstructor<TModule>
		components?: IComponentsConfig
		modules?: IModulesConfig
		workflows?: TWorkflowsConfig
		threads?: TThreadsConfig
		processes?: TProcessesConfig
		[propertyKey: string]: any
	}
}