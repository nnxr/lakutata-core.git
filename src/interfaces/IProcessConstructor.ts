import {IConstructor} from './IConstructor'

export interface IProcessConstructor<T> extends IConstructor<T> {
	CLASS_PATH: string
	CLASS_NAME: string
}