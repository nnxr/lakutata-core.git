import {IComponentsConfig} from './IComponentsConfig'
import {IModulesConfig} from './IModulesConfig'
import {TPluginsConfig} from '../types/TPluginsConfig'
import {Application} from '../Application'
import {TWorkflowsConfig} from '../types/TWorkflowsConfig'
import {TThreadsConfig} from '../types/TThreadsConfig'
import {TProcessesConfig} from '../types/TProcessesConfig'

export interface IApplicationConfig {
	id: string
	name: string
	timezone?: string
	plugins?: TPluginsConfig
	components?: IComponentsConfig
	modules?: IModulesConfig
	workflows?: TWorkflowsConfig<Application>
	threads?: TThreadsConfig
	processes?: TProcessesConfig
	onComponentsLoaded?: (app: Application) => void | Promise<void>
	onModulesLoaded?: (app: Application) => void | Promise<void>
}