import {IConstructor} from './IConstructor'

export interface IThreadConstructor<T> extends IConstructor<T> {
	CLASS_PATH: string
	CLASS_NAME: string
}