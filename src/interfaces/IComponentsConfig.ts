import {Component} from '../base/abstracts/Component'
import {IConstructor} from './IConstructor'

export interface IComponentsConfig<TComponent extends Component = Component> {
	[key: string]: {
		class: IConstructor<TComponent>
		[propertyKey: string]: any
	} | IConstructor<TComponent>
}