import {inject, injectable} from 'inversify'
import {CONFIGURABLE_NAMES} from '../constants/MetadataKeys'
export {inject as Inject, injectable as Injectable}

/**
 * 可配置属性字段装饰器
 * @returns {PropertyDecorator}
 * @constructor
 */
export function Configurable(): PropertyDecorator {
	return (target, propertyKey) => {
		if (!Reflect.getMetadata(CONFIGURABLE_NAMES, target)) {
			Reflect.defineMetadata(CONFIGURABLE_NAMES, [], target)
		}
		if (!Reflect.getMetadata(CONFIGURABLE_NAMES, target).includes(propertyKey)) Reflect.getMetadata(CONFIGURABLE_NAMES, target).push(propertyKey)
	}
}