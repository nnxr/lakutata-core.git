import {Plugin} from '../base/Plugin'
import {JSONParserV2} from 'json-helpers'
import {Exception} from '../base/abstracts/Exception'

declare module '../Core' {
	interface Application {
		JSON: JSON
	}
}

class ParseException extends Exception {
	constructor() {
		super('')
	}

	public district: string
	public errno: number
}

JSONParserV2.formatter<Exception | Error>({
	objectType: 'Error',
	objectConstructor: (Error as unknown) as ObjectConstructor,
	serialize: (t: Exception | Error) => {
		if (t instanceof Exception) {
			return {
				isException: true,
				appId: t.appId,
				appName: t.appName,
				errMsg: t.errMsg,
				err: t.err,
				errno: t.errno,
				stack: Buffer.from(t.stack ? t.stack : '').toString('base64')
			}
		} else {
			return {
				isException: false,
				name: t.name,
				message: t.message,
				stack: Buffer.from(t.stack ? t.stack : '').toString('base64')
			}
		}
	},
	unserialize: (parsedErrorObject: any) => {
		if (parsedErrorObject.isException) {
			const unSerializeException = class extends ParseException {
				public get name(): string {
					return parsedErrorObject.err
				}
			}
			return Object.assign(new unSerializeException(), {
				message: parsedErrorObject.errMsg,
				appId: parsedErrorObject.appId,
				appName: parsedErrorObject.appName,
				errMsg: parsedErrorObject.errMsg,
				err: parsedErrorObject.err,
				errno: parsedErrorObject.errno,
				stack: Buffer.from(parsedErrorObject.stack, 'base64').toString()
			})
		} else {
			const unSerializeError = new Error()
			unSerializeError.name = parsedErrorObject.name
			unSerializeError.message = parsedErrorObject.message
			unSerializeError.stack = Buffer.from(parsedErrorObject.stack, 'base64').toString()
			return unSerializeError
		}
	}
})

export class JSON extends Plugin {
	public stringify<T = any>(value: T, replacer?: (key: string, value: any) => any, space?: string | number): string {
		return JSONParserV2.stringify(value, replacer, space)
	}

	public parse<T = any>(text: string, reviver?: (key: string, value: any) => any): T {
		return JSONParserV2.parse(text, reviver)
	}
}
