import {Plugin} from '../base/Plugin'
import msgpack from 'msgpack5'
import {Buffer} from 'buffer'

declare module '../Core' {
	interface Application {
		MessagePack: MessagePack
	}
}

export class MessagePack extends Plugin {
	/**
	 * 编码数据
	 * @param {T} inp
	 * @returns {Buffer}
	 */
	public encode<T = any>(inp: T): Buffer {
		const hexData = msgpack().encode(inp).toString('hex')
		return Buffer.from(hexData, 'hex')
	}

	/**
	 * 解码数据
	 * @param {Buffer} data
	 * @returns {T}
	 */
	public decode<T = any>(data: Buffer): T {
		return msgpack().decode(data)
	}
}