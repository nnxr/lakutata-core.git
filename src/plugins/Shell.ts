import {Plugin} from '../base/Plugin'
import {
	CatFunction,
	ChmodFunction,
	CopyFunction,
	DirsFunction,
	EchoFunction,
	ExecFunction,
	FindFunction,
	GrepFunction,
	HeadFunction,
	LinkFunction,
	ListFunction,
	MkdirFunction,
	MoveFunction,
	PopDirFunction,
	PushDirFunction,
	RemoveFunction,
	SedFunction,
	ShellString,
	SortFunction,
	TailFunction,
	TestOptions,
	TouchFunction,
	UniqFunction
} from '../interfaces/ShellJsInterfaces'

const shelljs = require('../lib/minified/ShellJs')

declare module '../Core' {
	interface Application {
		Shell: Shell
	}
}

export class Shell extends Plugin {
	protected shelljs: any = shelljs

	/**
	 * Changes the current working directory dir for the duration of the script.
	 * Changes to the home directory if no argument is supplied.
	 *
	 * @param dir Directory to change to.
	 * @return    Object with shell exit code, stderr and stdout.
	 */
	public cd(dir?: string): ShellString {
		return this.shelljs.cd(dir)
	}

	/**
	 * Returns the current directory.
	 * @return The current directory.
	 */
	public pwd(): ShellString {
		return this.shelljs.pwd()
	}

	/**
	 * Evaluates expression using the available primaries and returns corresponding value.
	 *
	 * @param option Valid options:
	 *        - `-b`: true if path is a block device;
	 *        - `-c`: true if path is a character device;
	 *        - `-d`: true if path is a directory;
	 *        - `-e`: true if path exists;
	 *        - `-f`: true if path is a regular file;
	 *        - `-L`: true if path is a symbolic link;
	 *        - `-p`: true if path is a pipe (FIFO);
	 *        - `-S`: true if path is a socket
	 * @param path    The path.
	 * @return        See option parameter.
	 */
	public test(option: TestOptions, path: string): boolean {
		return this.shelljs.test(option, path)
	}

	/**
	 * Searches for command in the system's PATH. On Windows looks for .exe, .cmd, and .bat extensions.
	 *
	 * @param command The command to search for.
	 * @return        Returns string containing the absolute path to the command.
	 */
	public which(command: string): ShellString {
		return this.shelljs.which(command)
	}

	/**
	 * Exits the current process with the given exit code.
	 *
	 * Equivalent to calling `process.exit(code)`.
	 *
	 * @param code The exit code.
	 */
	public exit(code?: number): never {
		return this.shelljs.exit(code) as never
	}

	/**
	 * Searches and returns string containing a writeable, platform-dependent temporary directory.
	 * Follows Python's tempfile algorithm.
	 *
	 * @return The temp file path.
	 */
	public tempdir(): ShellString {
		return this.shelljs.tempdir()
	}

	/**
	 * Tests if error occurred in the last command.
	 *
	 * @return Returns null if no error occurred, otherwise returns string explaining the error
	 */
	public error(): ShellString {
		return this.shelljs.error()
	}

	/**
	 * Sets global configuration variables
	 * @param options Available options:
	 *        - `+/-e`: exit upon error (`config.fatal`),
	 *        - `+/-v`: verbose: show all commands (`config.verbose`),
	 *        - `+/-f`: disable filename expansion (globbing)
	 */
	public set(options: string): void {
		return this.shelljs.set(options)
	}

	/**
	 * Returns array of files in the given path, or in current directory if no path provided.
	 *
	 * @param options Available options:
	 *        - `-R`: recursive
	 *        - `-A`: all files (include files beginning with ., except for . and ..)
	 *        - `-L`: follow symlinks
	 *        - `-d`: list directories themselves, not their contents
	 *        - `-l`: list objects representing each file, each with fields containing
	 *                `ls -l` output fields. See fs.Stats for more info
	 * @param paths   Paths to search.
	 * @return        An array of files in the given path(s).
	 */
	public ls: ListFunction = this.shelljs.ls

	/**
	 * Returns array of all files (however deep) in the given paths.
	 *
	 * @param path The path(s) to search.
	 * @return     An array of all files (however deep) in the given path(s).
	 */
	public find: FindFunction = this.shelljs.find

	/**
	 * Copies files. The wildcard `*` is accepted.
	 *
	 * @param options Available options:
	 *        - `-f`: force (default behavior)
	 *        - `-n`: no-clobber
	 *        - `-u`: only copy if source is newer than dest
	 *        - `-r`, -R: recursive
	 *        - `-L`: follow symlinks
	 *        - `-P`: don't follow symlinks
	 * @param source The source.
	 * @param dest   The destination.
	 * @return       Object with shell exit code, stderr and stdout.
	 */
	public cp: CopyFunction = this.shelljs.cp

	/**
	 * Removes files. The wildcard `*` is accepted.
	 *
	 * @param options Available options:
	 *        - `-f` (force),
	 *        - `-r`, `-R` (recursive)
	 * @param files   Files to remove.
	 * @return        Object with shell exit code, stderr and stdout.
	 */
	public rm: RemoveFunction = this.shelljs.rm

	/**
	 * Moves files. The wildcard `*` is accepted.
	 *
	 * @param options Available options:
	 *        - `-f`: force (default behavior)
	 *        - `-n`: no-clobber
	 * @param source The source.
	 * @param dest   The destination.
	 * @return       Object with shell exit code, stderr and stdout.
	 */
	public mv: MoveFunction = this.shelljs.mv

	/**
	 * Creates directories.
	 *
	 * @param options Available options:
	 *        - `-p`: full paths, will create intermediate dirs if necessary
	 * @param dir     The directories to create.
	 * @return        Object with shell exit code, stderr and stdout.
	 */
	public mkdir: MkdirFunction = this.shelljs.mkdir

	/**
	 * Returns a string containing the given file, or a concatenated string
	 * containing the files if more than one file is given (a new line character
	 * is introduced between each file).
	 *
	 * @param files Files to use. Wildcard `*` accepted.
	 * @return A string containing the given file, or a concatenated string
	 *         containing the files if more than one file is given
	 *         (a new line character is introduced between each file).
	 */
	public cat: CatFunction = this.shelljs.cat

	/**
	 * Reads an input string from file and performs a JavaScript `replace()`
	 * on the input using the given search regex and replacement string or function.
	 *
	 * @param options Available options:
	 *        - `-i`: Replace contents of 'file' in-place. Note that no backups will be created!
	 * @param searchRegex The regular expression to use for search.
	 * @param replacement The replacement.
	 * @param files       The files to process.
	 * @return            The new string after replacement.
	 */
	public sed: SedFunction = this.shelljs.sed

	/**
	 * Reads input string from given files and returns a string containing all lines
	 * of the file that match the given `regex_filter`. Wildcard `*` accepted.
	 *
	 * @param options Available options:
	 *        - `-v`: Inverse the sense of the regex and print
	 *                the lines not matching the criteria.
	 *        - `-l`: Print only filenames of matching files
	 * @param regex_filter The regular expression to use.
	 * @param files        The files to process.
	 * @return Returns a string containing all lines of the file that match the given `regex_filter`.
	 */
	public grep: GrepFunction = this.shelljs.grep

	/**
	 * Prints string to stdout, and returns string with additional utility methods like .to().
	 *
	 * @param options Available options:
	 *        - `-e`: interpret backslash escapes (default)
	 *        - `-n`: remove trailing newline from output
	 * @param text The text to print.
	 * @return     Returns the string that was passed as argument.
	 */
	public echo: EchoFunction = this.shelljs.echo

	/**
	 * Saves the current directory on the top of the directory stack and then cd to dir.
	 * With no arguments, `pushd` exchanges the top two directories.
	 *
	 * @param options Available options:
	 *        - `-n`: Suppresses the normal change of directory when adding directories
	 *                to the stack, so that only the stack is manipulated
	 *        - `-q`: Suppresses output to the console.
	 * @param dir     Makes the current working directory be the top of the stack,
	 *                and then executes the equivalent of `cd dir`.
	 * @return        Returns an array of paths in the stack.
	 */
	public pushd: PushDirFunction = this.shelljs.pushd

	/**
	 * When no arguments are given, popd removes the top directory from the stack
	 * and performs a `cd` to the new top directory.
	 *
	 * The elements are numbered from 0 starting at the first directory listed with dirs;
	 * i.e., `popd` is equivalent to `popd +0`. Returns an array of paths in the stack.
	 *
	 * @param options Available options:
	 *        - `-n`: Suppresses the normal change of directory when removing directories
	 *                from the stack, so that only the stack is manipulated
	 *        - `-q`: Suppresses output to the console.
	 * @param dir     You can only use -N and +N.
	 * @return        Returns an array of paths in the stack.
	 */
	public popd: PopDirFunction = this.shelljs.popd

	/**
	 * Displays the list of currently remembered directories.
	 *
	 * @param options Available options:
	 *        - `-c`: Clears the directory stack by deleting all of the elements.
	 *        - `-N`: Displays the Nth directory (counting from the right of the list
	 *                printed by dirs when invoked without options), starting with zero.
	 *        - `+N`: Displays the Nth directory (counting from the left of the list
	 *                printed by dirs when invoked without options), starting with zero.
	 * @return        Returns an array of paths in the stack, or a single path if +N or -N was specified.
	 */
	public dirs: DirsFunction = this.shelljs.dirs

	/**
	 * Links source to dest. Use `-f` to force the link, should dest already exist.
	 *
	 * @param options Available options:
	 *        - `-s`: Create a symbolic link, defaults to a hardlink
	 *        - `-f`: Force creation
	 * @param source The source.
	 * @param dest   The destination.
	 * @return       Object with shell exit code, stderr and stdout.
	 */
	public ln: LinkFunction = this.shelljs.ln

	/**
	 * Executes the given command.
	 *
	 * @param command The command to execute.
	 * @param options Silence and synchronous options.
	 * @param [callback] Receives code and output asynchronously.
	 * @return Returns an object containing the return code and output as string,
	 *         or if `{async: true}` or a `callback` was passed, a `ChildProcess`.
	 */
	public exec: ExecFunction = this.shelljs.exec

	/**
	 * Alters the permissions of a file or directory by either specifying the absolute
	 * permissions in octal form or expressing the changes in symbols.
	 *
	 * This command tries to mimic the POSIX behavior as much as possible.
	 *
	 * Notable exceptions:
	 * - In symbolic modes, 'a-r' and '-r' are identical. No consideration is given to the umask.
	 * - There is no "quiet" option since default behavior is to run silent.
	 *
	 * @param options Available options:
	 *        - `-v`: output a diagnostic for every file processed
	 *        - `-c`: like -v but report only when a change is made
	 *        - `-R`: change files and directories recursively
	 * @param mode    The access mode. Can be an octal string or a symbolic mode string.
	 * @param file    The file to use.
	 * @return        Object with shell exit code, stderr and stdout.
	 */
	public chmod: ChmodFunction = this.shelljs.chmod

	/**
	 * Update the access and modification times of each FILE to the current time.
	 * A FILE argument that does not exist is created empty, unless `-c` is supplied
	 */
	public touch: TouchFunction = this.shelljs.touch

	/**
	 * Read the start of a file.
	 */
	public head: HeadFunction = this.shelljs.head

	/**
	 * Return the contents of the files, sorted line-by-line.
	 * Sorting multiple files mixes their content (just as unix sort does).
	 *
	 * @param options Available options:
	 *        - `-r`: Reverse the results
	 *        - `-n`: Compare according to numerical value
	 */
	public sort: SortFunction = this.shelljs.sort

	/**
	 * Read the end of a file.
	 */
	public tail: TailFunction = this.shelljs.tail

	/**
	 * Filter adjacent matching lines from input.
	 *
	 * @param options Available options:
	 *        - `-i`: Ignore case while comparing
	 *        - `-c`: Prefix lines by the number of occurrences
	 *        - `-d`: Only print duplicate lines, one for each group of identical lines
	 */
	public uniq: UniqFunction = this.shelljs.uniq
}
