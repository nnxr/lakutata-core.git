import {Plugin} from '../base/Plugin'
import {createLogger, Logger as WinstonLogger, transports} from 'winston'
import {Configurable} from '../decorators/DependencyInjection'
import * as Transport from 'winston-transport'
import {format as utilFormat} from 'util'
import {ConsoleTransportOptions} from 'winston/lib/winston/transports'
import momentTimezone from 'moment-timezone'
import {TimestampOptions, format} from 'logform'

declare module '../Core' {
	interface Application {
		Logger: Logger
	}
}

export class Logger extends Plugin {
	private logger: WinstonLogger

	@Configurable()
	protected readonly level: 'emerg' | 'alert' | 'crit' | 'error' | 'warning' | 'notice' | 'info' | 'debug' = 'debug'

	@Configurable()
	protected readonly meta: object

	@Configurable()
	protected readonly transports: Transport[] | Transport = []

	@Configurable()
	protected readonly console: ConsoleTransportOptions = {
		silent: false,
		format: format.combine(
			format.timestamp({
				format: () => {
					return momentTimezone.tz(Date.now(), <string>process.env.TZ).format('YYYY-MM-DD HH:mm:ss')
				}
			}),
			format.colorize(),
			format.printf(packet => {
				return `${packet.timestamp} ${packet.level}: ${packet.message}`
			})
		)
	}

	protected onActivation() {
		const loggerTransports = this.transports ? (Array.isArray(this.transports) ? this.transports : [this.transports]) : []
		let hasConsole: boolean = false
		for (const loggerTransport of loggerTransports) {
			if (loggerTransport instanceof transports.Console) {
				hasConsole = true
				break
			}
		}
		if (!hasConsole) {
			loggerTransports.unshift(new transports.Console(this.console))
		}
		this.logger = createLogger({
			level: this.level,
			defaultMeta: this.meta,
			levels: {
				emerg: 0,
				alert: 1,
				crit: 2,
				error: 3,
				warning: 4,
				notice: 5,
				info: 6,
				debug: 7
			},
			transports: loggerTransports
		})
	}

	/**
	 * 格式化日志输入参数
	 * @param {any[]} args
	 * @returns {string}
	 * @protected
	 */
	protected formatArguments(args: any[]): string {
		return utilFormat.apply(utilFormat, args)
	}

	/**
	 * 紧急
	 * @param args
	 * @description 系统不可用
	 */
	public emerg(...args: any[]): void {
		this.logger.emerg(this.formatArguments(args))
	}

	/**
	 * 报警
	 * @param args
	 * @description 必须马上采取行动的事件
	 */
	public alert(...args: any[]) {
		this.logger.alert(this.formatArguments(args))
	}

	/**
	 * 关键
	 * @param args
	 * @description 关键的事件
	 */
	public crit(...args: any[]) {
		this.logger.crit(this.formatArguments(args))
	}

	/**
	 * 错误
	 * @param args
	 * @description 错误事件
	 */
	public error(...args: any[]) {
		this.logger.error(this.formatArguments(args))
	}

	/**
	 * 警告
	 * @param args
	 * @description 警告事件
	 */
	public warning(...args: any[]) {
		this.logger.warning(this.formatArguments(args))
	}

	/**
	 * 通知
	 * @param args
	 * @description 普通但重要的事件
	 */
	public notice(...args: any[]) {
		this.logger.notice(this.formatArguments(args))
	}

	/**
	 * 消息
	 * @param args
	 * @description 有用的信息
	 */
	public info(...args: any[]) {
		this.logger.info(this.formatArguments(args))
	}

	/**
	 * 调试
	 * @param args
	 * @description 调试信息
	 */
	public debug(...args: any[]) {
		this.logger.debug(this.formatArguments(args))
	}
}