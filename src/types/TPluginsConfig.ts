import {IConstructor} from '../interfaces/IConstructor'
import {Plugin} from '../base/Plugin'

export type TPluginsConfig<TPlugin extends Plugin = Plugin> =
	({ class: IConstructor<TPlugin>, scope?: 'Transient' | 'Singleton'; [propertyKey: string]: any } | IConstructor<TPlugin>)[]