import {Module} from '../base/abstracts/Module'
import {Workflow} from '../base/abstracts/Workflow'
import {IConstructor} from '../interfaces/IConstructor'

export type TWorkflowsConfig<TModule extends Module = Module> =
	(((instance: TModule) => void) | ((instance: TModule) => Promise<void>) | IConstructor<Workflow>)[]