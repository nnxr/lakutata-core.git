import {Process} from '../base/process/Process'
import {IProcessConstructor} from '../interfaces/IProcessConstructor'

export type TProcessesConfig = IProcessConstructor<Process>[]