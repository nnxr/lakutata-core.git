import {Thread} from '../base/thread/Thread'
import {IThreadConstructor} from '../interfaces/IThreadConstructor'

export type TThreadsConfig = IThreadConstructor<Thread>[]