import momentTimezone from 'moment-timezone'

require('events').captureRejections = true
import {IApplicationConfig} from './interfaces/IApplicationConfig'
import {Application} from './Application'
import {FatalException} from './exceptions/FatalException'
import {Container} from './base/Container'
import {CORE_CONTAINER_ID} from './constants/MetadataKeys'

if (!Reflect.getMetadata(CORE_CONTAINER_ID, process)) Reflect.defineMetadata(CORE_CONTAINER_ID, new Container(), process)

/**
 * 创建程序
 * @param {IApplicationConfig} config
 * @returns {Promise<Application>}
 */
export async function createApp(config: IApplicationConfig): Promise<Application> {
	try {
		const coreContainer: Container = Reflect.getMetadata(CORE_CONTAINER_ID, process)
		coreContainer.bindModule(Application, config)
		process.title = config.id
		process.env.appId = config.id
		process.env.appName = config.name
		process.env.TZ = config.timezone ? config.timezone : momentTimezone.tz.guess()
		await coreContainer.initialize()
		return coreContainer.getModule(Application)
	} catch (e) {
		const fatalException = new FatalException((e as Error).message)
		const createAppErrorStack = (e as Error).stack
		if (createAppErrorStack) {
			fatalException.stack = createAppErrorStack
		}
		throw fatalException
	}
}

/**
 * 获取程序
 * @returns {Application | null}
 */
export function getApp(): Application {
	try {
		const coreContainer: Container = Reflect.getMetadata(CORE_CONTAINER_ID, process)
		return coreContainer.getModule(Application)
	} catch (e) {
		throw new FatalException('Application is not launched')
	}
}
