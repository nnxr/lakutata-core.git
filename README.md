<div style="text-align:center">
	<img src="https://gitee.com/nnxr/lakutata-core/raw/master/assets/lakutata-banner.png" />
</div>

[![NPM Version](https://img.shields.io/npm/v/@lakutata/core?color=informational&style=flat-square)](https://npmjs.org/package/@lakutata/core)
[![NODE Version](https://img.shields.io/node/v/@lakutata/core?color=informational&style=flat-square)](https://npmjs.org/package/@lakutata/core)
[![Known Vulnerabilities](https://snyk.io/test/npm/@lakutata/core/badge.svg?style=flat-square)](https://snyk.io/test/npm/@lakutata/core)
[![NPM Download](https://img.shields.io/npm/dm/@lakutata/core?style=flat-square)](https://npmjs.org/package/@lakutata/core)

## TODO

- Write document for Lakutata

## Features

- TODO

## Quickstart

Follow the commands listed below.

```bash
$ mkdir example && cd example
$ TODO
```

> Node.js >= 12.22.1 required.

## Documentations

- TODO

## How to Contribute

Please let us know how can we help. Do check out [issues](https://gitee.com/nnxr/lakutata-core/issues) for bug reports or suggestions first.

## License

[MIT](LICENSE)